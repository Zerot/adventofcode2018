module Day4

open System.Text.RegularExpressions
open System

type Line = {
  time: DateTime
  action: string
}

type Guard = {
  id: int
  sleeps: (DateTime * DateTime) list
}

let lineRegex = Regex("""\[(.*)\] (.*)""")
let beginShiftRegex = Regex("""Guard #(\d+) begins shift""")
let fallsAsleepRegex = Regex("""falls asleep""")

let (|BeginShift|FallsAsleep|WakeUp|) s =
  let m = beginShiftRegex.Match s
  if m.Success then
    BeginShift (int m.Groups.[1].Value)
  else
    let m = fallsAsleepRegex.Match s
    if m.Success then
      FallsAsleep
    else
      WakeUp

let data =
  System.IO.File.ReadAllLines("input.txt")
  |> Seq.map (fun line -> 
    let m = lineRegex.Match(line)
    {
      time = DateTime.Parse(m.Groups.[1].Value)
      action = m.Groups.[2].Value
    }
  )

let solver data =
  data
  |> Seq.sortBy(fun t -> t.time)
  |> Seq.fold (fun (lastId, fellAsleep, state) e -> 
    match e.action with
    | BeginShift id ->
      (id, fellAsleep, state)
    | FallsAsleep ->
      (lastId, e.time, state)
    | WakeUp ->
      let g =
        state 
        |> Map.tryFind lastId
        |> Option.defaultValue {
          id = lastId
          sleeps = []
        }
      let moment = (fellAsleep, e.time)
      let newG = { g with sleeps = (moment :: g.sleeps) }
      (lastId, DateTime.MinValue, state |> Map.add lastId newG)
  ) (0, DateTime.MinValue, Map.empty)
  |> (fun (_,_,m) -> m |> Map.toSeq |> Seq.map snd)
  |> Seq.map (fun g ->
    let pattern =
      g.sleeps
      |> List.collect (fun (s, e) ->
        [(s.Minute, +1); (e.Minute, -1)]
      )
      |> List.sortBy fst
      |> List.fold (fun (maxV, maxT, cur) (t, v) ->
        let newCur = cur + v
        if newCur > maxV then
          (newCur, t, newCur)
        else
          (maxV, maxT, newCur)
      ) (0, 0, 0)
    let tot =
      g.sleeps
      |> List.map (fun (s,e) -> e.Minute - s.Minute)
      |> List.reduce (+)
    (g.id, tot, pattern)
  )

let part1 () =
  solver data
  |> Seq.sortByDescending (fun (_,s,_) -> s)

let part2 () =
  solver data
  |> Seq.sortByDescending (fun (_,_,(m,_,_)) -> m)


[<EntryPoint>]
let main _ =
  let s = System.Diagnostics.Stopwatch()
  s.Start()
  let result = part2 ()
  s.Stop()
  printfn "Result %A\nTime %dms" result s.ElapsedMilliseconds
  0 // return an integer exit code
