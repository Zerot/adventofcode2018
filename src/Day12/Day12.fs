module Day12
open System.Collections.Generic

let initialState =
  System.IO.File.ReadAllText("initialstate.txt")
  |> Seq.mapi (fun i c ->
    match c with
    | '#' -> (i, true, false)
    | _ -> (i, false, false)
  )
  |> Seq.toList
  |> (fun x ->
    LinkedList<int*bool*bool>(x)
  )

let rules =
  System.IO.File.ReadAllLines("input.txt")
  |> Seq.map (fun l ->
    let input =
      l.[0..4]
      |> Seq.map (
        function
        | '#' -> true
        | _ -> false
      )
      |> Seq.toList
    let output = 
      match l.[9] with
      | '#' -> true
      | _ -> false
    (input, output)
  )
  |> Map.ofSeq

let fst3 (x,_,_) = x
let snd3 (_,x,_) = x
let thd3 (_,_,x) = x

let step (field: LinkedList<int*bool*bool>) =
  // ensure first 2 entries are empty
  if snd3 field.First.Value then
    field.AddFirst(((fst3 field.First.Value) - 1, false, false)) |> ignore
    field.AddFirst(((fst3 field.First.Value) - 1, false, false)) |> ignore
  else if (snd3 field.First.Next.Value) then
    field.AddFirst(((fst3 field.First.Value) - 1, false, false)) |> ignore

  // ensure last 2 entries are empty
  if snd3 field.Last.Value then
    field.AddLast(((fst3 field.Last.Value) + 1, false, false)) |> ignore
    field.AddLast(((fst3 field.Last.Value) + 1, false, false)) |> ignore
  else if (snd3 field.Last.Previous.Value) then
    field.AddLast(((fst3 field.Last.Value) + 1, false, false)) |> ignore
  
  let applyRules (n: LinkedListNode<int*bool*bool>) =
    // iterate rules
    let key =
      [ not (isNull n.Previous) && not (isNull n.Previous.Previous) && snd3 n.Previous.Previous.Value
        not (isNull n.Previous) && snd3 n.Previous.Value
        snd3 n.Value
        not (isNull n.Next) && snd3 n.Next.Value
        not (isNull n.Next) && not (isNull n.Next.Next) && snd3 n.Next.Next.Value
      ]
    let rule = rules |> Map.find key
    n.Value <- (fst3 n.Value, snd3 n.Value, rule)

  // iterate over field
  let rec iter (f: LinkedListNode<int*bool*bool> -> unit) (n: LinkedListNode<int*bool*bool>) =
    f n
    if not <| isNull n.Next then
      iter f n.Next
  
  iter applyRules field.First

  // move generation over
  iter (fun n -> n.Value <- (fst3 n.Value, thd3 n.Value, false)) field.First

  iter (fun n -> 
    printf "%3d" (fst3 n.Value)
    if snd3 n.Value then printf "#" else printf ".") field.First
  printf "\n"

  field

let part1 () =
  [0..199]
  |> Seq.fold (fun s _ -> 
    step s
  ) initialState
  |> Seq.fold (fun s (i, f, _) -> if f then s+i else s) 0

[<EntryPoint>]
let main argv =
  let s = System.Diagnostics.Stopwatch()
  s.Start()
  let result = part1 ()
  s.Stop()
  printfn "Time: %dms, Result: %A" s.ElapsedMilliseconds result
  0 // return an integer exit code
