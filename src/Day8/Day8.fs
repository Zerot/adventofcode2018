module Day8

let data =
  System.IO.File.ReadAllText("input.txt")
  |> (fun s -> s.Split(" "))
  |> Seq.map int
  |> Seq.toList

type Node = {
  children: Node list
  metadata: int list
}

let rec buildTree (data: int list): (Node * (int list)) =
  let childCount :: metadataCount :: remainingData = data
  match childCount with
  | 0 -> ({ children = []; metadata = remainingData |> List.take metadataCount }, remainingData |> List.skip metadataCount)
  | x ->
    [0..x-1]
    |> List.fold (fun (cs, d) i ->
      let (child, nd) = buildTree d
      (cs @ [child], nd)
    ) ([], remainingData)
    |> fun (cs, d) -> ({ children = cs; metadata = d |> List.take metadataCount}, d |> List.skip metadataCount)

let part1() =
  let rec countMetadata tree =
    tree.metadata
    |> List.sum
    |> (fun s -> 
      s + (tree.children |> List.fold (fun a c -> a + countMetadata c) 0)
    )

  buildTree data
  |> fst
  |> countMetadata

let part2() =
  let rec getValue node =
    match node.children with
    | [] -> List.sum node.metadata
    | cs ->
      node.metadata
      |> List.fold (fun a i ->
        let c = cs |> List.tryItem (i-1)
        match c with
        | Some s -> a + getValue s
        | None -> a
      ) 0
  
  buildTree data
  |> fst
  |> getValue


[<EntryPoint>]
let main argv =
  let s = System.Diagnostics.Stopwatch()
  s.Start()
  let result = part1()
  s.Stop()
  printfn "Time: %dms, Result: %A" s.ElapsedMilliseconds result
  0 // return an integer exit code
