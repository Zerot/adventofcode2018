module Day11

let data = 5093

let part1() =
  seq {
    for x in [0..297] do
      for y in [0..297] do
        let mutable total = 0
        for x2 in [0..2] do
          for y2 in [0..2] do
            let rackId = (x+x2) + 10
            let level =
              (y+y2) * rackId
              |> (+) data
              |> (*) rackId
              |> fun level -> level / 100
              |> fun level -> level % 10
              |> fun level -> level - 5
            total <- total + level
        yield (x,y,total)
  }
  |> Seq.sortByDescending (fun (_,_,x) -> x)


let drawBoard (board: int[]) =
  for y in [0..299] do
    for x in [0..299] do
      printf "%3d " board.[x+y*300]
    printf "\n"

let summedAreaTable () =
  let board =
    seq {
      for x in [0..299] do
        for y in [0..299] do
          let rackId = x + 10
          yield
            y * rackId
            |> (+) data
            |> (*) rackId
            |> fun level -> level / 100
            |> fun level -> level % 10
            |> fun level -> level - 5
    } 
    |> Seq.toArray

  // drawBoard board
  // printfn "------"

  for x in [1..299] do
    for y in [0..299] do
      board.[x+y*300] <- board.[x+y*300] + board.[x-1+y*300]
  // drawBoard board
  // printfn "------"

  for y in [1..299] do
    for x in [0..299] do
      board.[x+y*300] <- board.[x+y*300] + board.[x+(y-1)*300]
  // drawBoard board
  // printfn "------"
  board



let part2() =
  let board = summedAreaTable()
  seq {
    for i in [0..299] do
      for x in [i..299] do
        for y in [i..299] do
          let sum =
            board.[(x-i)+(y-i)*300]
            + board.[x+y*300]
            - board.[(x)+(y-i)*300]
            - board.[(x-i)+(y)*300]
          yield (y-i,x-i,i,sum)
  }
  |> Seq.sortByDescending (fun (_,_,_,x) -> x)


[<EntryPoint>]
let main _ =
  let s = System.Diagnostics.Stopwatch()
  s.Start()
  let result = part2 ()
  s.Stop()
  printfn "Time: %dms, Result: %A" s.ElapsedMilliseconds result
  0 // return an integer exit code
