module Day7

open System.Text.RegularExpressions
open System

let lineRegex = Regex("""Step (\w) must be finished before step (\w) can begin.""", RegexOptions.Compiled)

let data =
  System.IO.File.ReadAllLines("input.txt")
  |> Seq.map (fun l ->
    let m = lineRegex.Match(l)
    (char m.Groups.[1].Value, char m.Groups.[2].Value)
  )

let rec collapse = 
  function
  | [(x, [])] -> [x]
  | xs ->
    let c =
      xs 
      |> List.filter (fun (x, r) -> List.length r = 0)
      |> List.sortBy (fun (x, r) -> x)
      |> List.head
      |> fst
    let newXs =
      xs
      |> List.filter (fun (x, r) -> x <> c)
      |> List.map (fun (x, r) -> (x, r |> List.filter (fun i -> i <> c)))
    c :: collapse newXs

let part1 () =
  data
  |> Seq.fold (fun s (r, d) ->
    let ds = Map.tryFind d s |> Option.defaultValue []
    let rs = Map.tryFind r s |> Option.defaultValue []
    s
    |> Map.add d (r :: ds)
    |> Map.add r rs
  ) Map.empty
  |> Map.toList
  |> collapse
  |> List.map string
  |> String.concat ""

let rec collapse2 active duration (remaining: (char * char list) list) =
  if remaining = [] then
    active
    |> List.fold (fun s (_, d) -> s+d) 0
    |> (fun x -> x + duration)
  else
    let takeFirst l =
      if List.length l = 0 then
        []
      else
        let d = l |> List.head |> snd
        l |> List.takeWhile (fun (_, xd) -> d = xd)

    let completed =
      active
      |> List.sortBy (fun (x, d) -> d)
      |> takeFirst

    let addActiveItems (remaining: (char * char list) list) oldActive =
      let available =
        remaining
        |> List.filter (fun (x, r) -> List.length r = 0)
        |> List.sortBy (fun (x, r) -> x)
        |> List.map fst
      let slotsOpen = 5 - List.length oldActive
      let starting =
        available
        |> List.truncate slotsOpen
        |> List.map (fun x -> (x, int x - int 'A' + 61))
      (oldActive @ starting, remaining |> List.filter (fun (rx, _) -> not <| List.exists (fun (sx, _) ->  rx = sx) starting))
    
    let newRemaining =
      remaining
      |> List.map (fun (x, r) -> (x, r |> List.filter (fun i -> not <| List.exists (fun (cx, _) -> i = cx) completed)))

    let d =
      if List.length completed = 0 then
        0
      else
        completed |> List.head |> snd

    let (newActive, newRemaining2) =
      active
      |> List.filter (fun (ix, id) -> not <| List.exists (fun (cx, _) -> ix = cx) completed)
      |> List.map (fun (ix, id) -> (ix, id-d))
      |> addActiveItems newRemaining

    collapse2 newActive (duration+d) newRemaining2

let part2 () =
  data
  |> Seq.fold (fun s (r, d) ->
    let ds = Map.tryFind d s |> Option.defaultValue []
    let rs = Map.tryFind r s |> Option.defaultValue []
    s
    |> Map.add d (r :: ds)
    |> Map.add r rs
  ) Map.empty
  |> Map.toList
  |> collapse2 [] 0

[<EntryPoint>]
let main argv =
  let s = System.Diagnostics.Stopwatch()
  s.Start()
  let result = part2 ()
  s.Stop()
  printfn "Time: %dms, Result: %A" s.ElapsedMilliseconds result
  0 // return an integer exit code
