module Day6

open System.Text.RegularExpressions
open System
open System.Drawing
open System.Collections.Generic
open System.Drawing.Imaging

type Id = Int32
type Distance = Int32

type Pos =
  | Empty
  | Claimed of Id * Distance
  | Contested of Distance

let data =
  System.IO.File.ReadAllLines("input.txt")
  |> Seq.map(fun l ->
    let split = l.Split([|',';' '|], System.StringSplitOptions.RemoveEmptyEntries)
    (int split.[0], int split.[1])
  )

let renderField result =
  let width = Array2D.length2 result
  result |> Array2D.iteri (fun x y p ->
    match p with
    | Empty -> printf "E   "
    | Claimed (id, d) -> printf "%1d:%1d " id d
    | Contested _ -> printf "C   "
    if (y=width-1) then
      printf "\n"
  )

let renderFieldToBitmap result =
  let width = Array2D.length1 result
  let height = Array2D.length2 result
  use bmp = new Bitmap(width, height)

  let colors = Dictionary<int, Color>()
  let rand = new Random()
  
  for x in [0 .. width-1] do
    for y in [0 .. height-1] do
      let pos = result.[x,y]
      let color =
        match pos with
        | Empty -> Color.Black
        | Contested _ -> Color.Red
        | Claimed (id,_) ->
          let success, cid = colors.TryGetValue(id)
          if not success then
            let newColor = Color.FromArgb(rand.Next(255), rand.Next(255), rand.Next(255))
            colors.[id] <- newColor
            newColor
          else
            cid
      bmp.SetPixel(x,y,color)
  bmp.Save("output.png", ImageFormat.Png)

let part1 () =
  let (left, right, top, bottom) =
    data
    |> Seq.fold (fun (minX, maxX, minY, maxY) (x,y) ->
      (min minX x, max maxX (x+1), min minY y, max maxY (y+1))
    ) (1000, 0, 1000, 0)

  let width = right - left
  let height = bottom - top

  let coords =
    data
    |> Seq.map (fun (x,y) -> (x-left, y-top))

  let field = Array2D.create (width) (height) Empty

  let filledField =
    coords
    |> Seq.fold (fun (field, id) (x,y) -> 
      let newField = 
        field |> Array2D.mapi (fun mx my pos ->
          let dist = abs (mx - x) + abs (my - y)
          match pos with
          | Empty -> Claimed (id, dist)
          | Claimed (oid, d) ->
            if dist < d then
              Claimed (id, dist)
            else if dist = d then
              Contested dist
            else
              Claimed (oid, d)
          | Contested d ->
            if dist < d then
              Claimed (id, dist)
            else
              Contested d
        )
      (newField, id+1)
    ) (field, 1)
    |> fst

  renderFieldToBitmap filledField
  
  let infiniteIds =
    let getId p =
      match p with
      | Empty -> -1
      | Claimed (id, _) -> id
      | Contested _ -> -2
    seq {
      for x in [0 .. right-left-1] do
        yield (filledField.[x, 0] |> getId)
        yield (filledField.[x, bottom - top - 1] |> getId)
      for y in [0 .. bottom-top-1] do
        yield (filledField.[0, y] |> getId)
        yield (filledField.[right - left - 1, y] |> getId)
    }
    |> Seq.distinct

  let counts =
    seq {
      for x in [0 .. width-1] do
        for y in [0 .. height-1] do
          yield filledField.[x,y]
    }
    |> Seq.fold (fun s i ->
      match i with
      | Empty -> s
      | Contested _ -> s
      | Claimed (id, _) ->
        let count = Map.tryFind id s |> Option.defaultValue 0
        s |> Map.add id (count + 1)
    ) Map.empty
  
  counts
  |> Map.toSeq
  |> Seq.filter (fun (i,_) -> not <| Seq.exists (fun i2 -> i = i2) infiniteIds)
  |> Seq.sortByDescending snd



let part2 () =
  let (left, right, top, bottom) =
    data
    |> Seq.fold (fun (minX, maxX, minY, maxY) (x,y) ->
      (min minX x, max maxX (x+1), min minY y, max maxY (y+1))
    ) (1000, 0, 1000, 0)

  let width = right - left
  let height = bottom - top

  let coords =
    data
    |> Seq.map (fun (x,y) -> (x-left, y-top))

  let field = Array2D.create (width) (height) 0

  let filledField =
    coords
    |> Seq.fold (fun (field, id) (x,y) -> 
      let newField = 
        field |> Array2D.mapi (fun mx my pos ->
          let dist = abs (mx - x) + abs (my - y)
          pos + dist
        )
      (newField, id+1)
    ) (field, 1)
    |> fst

  let count =
    seq {
      for x in [0 .. width-1] do
        for y in [0 .. height-1] do
          yield filledField.[x,y]
    }
    |> Seq.fold (fun s i ->
      if i < 10000 then
        s+1
      else
        s
    ) 0

  count

[<EntryPoint>]
let main _ =
    let s = System.Diagnostics.Stopwatch()
    s.Start()
    let result = part1()
    s.Stop()
    printfn "time: %dms" s.ElapsedMilliseconds
    //printfn "Result: %A" (result)
    0 // return an integer exit code
