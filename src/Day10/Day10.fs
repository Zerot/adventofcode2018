module Day10

open System.Text.RegularExpressions
open System
open System.Drawing
open System.Drawing.Imaging

let lineRegex = Regex("""position=<([- ]\d+), ([- ]\d+)> velocity=<([- ]\d+), ([- ]\d+)>""", RegexOptions.Compiled)

let data =
  System.IO.File.ReadAllLines "input.txt"
  |> Array.map (fun l ->
    let m = lineRegex.Match l
    ( int m.Groups.[1].Value
    , int m.Groups.[2].Value
    , int m.Groups.[3].Value
    , int m.Groups.[4].Value
    )
  )

let step: (int*int*int*int)[] -> (int*int*int*int)[] =
  Array.map (fun (x,y,vx,vy) ->
    (x+vx, y+vy, vx, vy)
  )

let draw filename data =
  let (left, right, top, bottom) =
    data 
    |> Array.fold (fun (minX, maxX, minY, maxY) (x,y, _, _) ->
      (min minX x, max maxX x, min minY y, max maxY y)
    ) (Int32.MaxValue, Int32.MinValue,Int32.MaxValue, Int32.MinValue)
  let width = right - left+1
  let height = bottom - top+1
  use bmp = new Bitmap(width, height)

  for (x,y,_,_) in data do
    bmp.SetPixel(x-left,y-top,Color.White)
  bmp.Save(sprintf "%s.png" filename, ImageFormat.Png)

let part1() =
  let start = // Line intersection shows that they will get near eachother after around 10500 steps
    [0..10500]
    |> List.fold (fun s _ -> step s) data
  [0..100]
  |> Seq.fold (fun s i ->
    let newData = step s
    draw (sprintf "test%03d" i) newData
    newData
  ) start

[<EntryPoint>]
let main argv =
  let s = System.Diagnostics.Stopwatch()
  s.Start()
  let result = part1 ()
  s.Stop()
  printfn "Time: %dms, Result: %A" s.ElapsedMilliseconds result
  0 // return an integer exit code