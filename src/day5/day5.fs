module Day5

let data = System.IO.File.ReadAllText "input.txt" |> Seq.map int

let collapse data = 
  data
  |> Seq.fold (fun p c -> 
    match p with
    | x :: xs when abs (x - c) = 32 -> xs
    | xs -> c::xs
  ) []

let part1 () =
  data
  |> collapse
  |> Seq.length

let part2 () =
  let newData = collapse data
  [(int 'a') .. (int 'z')]
  |> Seq.map (fun c ->
    newData
    |> Seq.filter (fun c2 -> c2 <> c && (c2+32) <> c)
    |> collapse
    |> Seq.length
  )
  |> Seq.min

[<EntryPoint>]
let main _ =
  let s = System.Diagnostics.Stopwatch()
  s.Start()
  let result = part2()
  s.Stop()
  printfn "result: %d, time: %dms" result s.ElapsedMilliseconds
  0 // return an integer exit code
