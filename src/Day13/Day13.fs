module Day13
open System.Drawing
open System.Drawing.Imaging

type Direction =
  | North
  | East
  | South
  | West

let inverseDirection =
  function
  | North -> South
  | East -> West
  | South -> North
  | West -> East

let rotateLeft =
  function
  | North -> West
  | East -> North
  | South -> East
  | West -> South

let rotateRight =
  function
  | North -> East
  | East -> South
  | South -> West
  | West -> North

type Ground =
  | Empty
  | Vertical
  | Horizontal
  | Corner of Direction*Direction
  | Intersection

type Position = int * int

type Switch =
  | Left
  | Straight
  | Right

type Train = {
  position: Position
  direction: Direction
  nextSwitch: Switch
  collided: bool
}

let createTrain position direction =
  { position = position
    direction = direction
    nextSwitch = Left
    collided = false
  }

let (field, trains) =
  System.IO.File.ReadAllLines("input.txt")
  |> Array.fold (fun (f, t, y) l ->
    let (lf, _, lt, _) =
      l |> Seq.fold (fun (s, p, lt, x) c ->
        match c with
        | ' ' -> (s @ [Empty], Empty, lt, x+1)
        | '-' -> (s @ [Horizontal], Horizontal, lt, x+1)
        | '|' -> (s @ [Vertical], Vertical, lt, x+1)
        | '/' ->
          let corner =
            if p = Horizontal || p = Intersection then
              Corner (West, North)
            else
              Corner (East, South)
          (s @ [corner], corner, lt, x+1)
        | '\\' ->
          let corner = 
            if p = Horizontal || p = Intersection then
              Corner (West, South)
            else
              Corner (East, North)
          (s @ [corner], corner, lt, x+1)
        | '+' ->  (s @ [Intersection], Intersection, lt, x+1)
        | '>' -> (s @ [Horizontal], Horizontal, createTrain (x,y) East :: lt, x+1)
        | '<' -> (s @ [Horizontal], Horizontal, createTrain (x,y) West :: lt, x+1)
        | '^' -> (s @ [Vertical], Vertical, createTrain (x,y) North :: lt, x+1)
        | 'v' -> (s @ [Vertical], Vertical, createTrain (x,y) South :: lt, x+1)
        | c -> 
          printfn "%A" c
          failwith "Found unknown character"
      ) ([], Empty, [], 0)

    (f @ [lf |> List.toArray], t @ lt, y+1)
  ) ([], [], 0)
  |> (fun (f, t, _) -> 
    (f |> List.toArray, t |> List.toArray)
  )

let drawBoard index (field: Ground[][]) (trains: Train[]) =
  let bitmap = new Bitmap(Array.length field.[1], Array.length field)
  field
  |> Array.iteri (fun y l ->
    l |> Array.iteri (fun x g ->
      match g with
      | Empty -> bitmap.SetPixel(x,y, Color.Black)
      | Horizontal | Vertical | Intersection | Corner _ -> bitmap.SetPixel(x,y,Color.DarkGray)
    )
  )

  trains |> Array.iter (fun t ->
    let (x,y) = t.position
    bitmap.SetPixel(x,y,Color.Red)
  )

  bitmap.Save(sprintf "output/%d.png" index, ImageFormat.Png)


drawBoard 0 field trains

let stepTrain train =
  let (oldX, oldY) = train.position
  let (newX, newY) =
    match train.direction with
    | East -> (oldX + 1, oldY)
    | West -> (oldX - 1, oldY)
    | North -> (oldX, oldY - 1)
    | South -> (oldX, oldY + 1)
  
  //printfn "Old: %d,%d; New: %d,%d" oldX oldY newX newY

  let ground =
    field.[newY].[newX]
  
  match ground with
  | Empty -> failwith "Derail"
  | Horizontal | Vertical -> {train with position = (newX, newY)}
  | Corner (d1, d2) ->
    let newDirection =
      if inverseDirection d1 = train.direction then
        d2
      else if inverseDirection d2 = train.direction then
        d1
      else
        failwith (sprintf "Derail %A" train)
    {train with 
      position = (newX, newY)
      direction = newDirection
    }
  | Intersection ->
    let newDirection =
      match train.nextSwitch with
      | Left -> rotateLeft train.direction
      | Straight -> train.direction
      | Right -> rotateRight train.direction
    let newSwitch =
      match train.nextSwitch with
      | Left -> Straight
      | Straight -> Right
      | Right -> Left
    {train with 
      position = (newX, newY)
      direction = newDirection
      nextSwitch = newSwitch
    }

let checkCollision (trains: Train[]) index =
  let train = trains.[index]
  let tpos = trains.[index].position
  if train.collided then
    None
  else
    trains
    |> Array.tryFindIndex (fun t ->
      t <> train && t.position = tpos
    )

let part1 () =
  let mutable trains = trains
  let mutable i = 1
  while trains |> Array.length > 1 do
    trains <-
      trains
      |> Array.sortBy (fun t -> snd t.position*10000 + fst t.position)
    for t in [0..Array.length trains - 1] do
      let train = trains.[t]
      trains.[t] <- stepTrain train
      let collision = checkCollision trains t

      collision
      |> Option.iter (fun ct -> 
        printfn "Collision %A" trains.[t]
        trains.[t] <- {trains.[t] with collided=true}
        trains.[ct] <- {trains.[ct] with collided=true}
      )

    trains <-
      trains
      |> Array.filter (fun t -> not t.collided)
    //drawBoard i field trains
    i <- i+1
    //printfn "Trains: %A" trains
  trains

[<EntryPoint>]
let main argv =
  let s = System.Diagnostics.Stopwatch()
  s.Start()
  let result = part1 ()
  s.Stop()
  printfn "Time: %dms, Result: %A" s.ElapsedMilliseconds result
  0 // return an integer exit code
