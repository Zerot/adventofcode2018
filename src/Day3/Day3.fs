module Day3
open System.Text.RegularExpressions

type Line = {
  id: int
  x: int
  y: int
  width: int
  height: int
}

let lineRegex = Regex("#(\d+) @ (\d+),(\d+): (\d+)x(\d+)", RegexOptions.Compiled)

let lineToRecord s =
  let m = lineRegex.Match s
  if not m.Success then
    None
  else
    Some
      { id = int (m.Groups.[1].Value)
        x = int (m.Groups.[2].Value)
        y = int (m.Groups.[3].Value)
        width = int (m.Groups.[4].Value)
        height = int (m.Groups.[5].Value)
      }

let count (a:int [,]) =
  let mutable c = 0
  a |> Array2D.iter (fun x -> if x>1 then c<-c+1)
  c

let part1 data = 
  let result =
    data
    |> Seq.fold (
      fun (state: int [,]) line ->
        for x in line.x .. (line.x+line.width-1) do
          for y in line.y .. (line.y+line.height-1) do
            state.[x,y] <- state.[x,y]+1
        state
    ) (Array2D.create 1000 1000 0)
  printfn "%A" (result |> count)

let part2 data =
  data
  |> Seq.find (fun line ->
    let collision =
      data
      |> Seq.exists (fun line2 ->
        line.id <> line2.id &&
        line.x                 < line2.x + line2.width &&
        line.x + line.width    > line2.x &&
        line.y                 < line2.y + line2.height &&
        line.y + line.height   > line2.y
      )
    not collision
  )
  |> printfn "%A"

[<EntryPoint>]
let main _ =
    let data =
      System.IO.File.ReadAllLines "input.txt"
      |> Seq.map lineToRecord
      |> Seq.choose id

    part2 data
    
    0 // return an integer exit code
