module Day14

let data = 540391
let data2 = [5;4;0;3;9;1]

let scoreAmount = 10

let part1() =
  seq {
    let prev = ResizeArray()
    prev.Add(3)
    prev.Add(7)
    let mutable cur1 = 0
    let mutable cur2 = 1
    yield 3
    yield 7
    while true do
      let sum = prev.[cur1] + prev.[cur2]
      if sum > 9 then
        prev.Add (sum / 10)
        yield (sum / 10)
        prev.Add (sum % 10)
        yield (sum % 10)
      else
        prev.Add (sum)
        yield sum
      cur1 <- (cur1 + 1 + prev.[cur1]) % prev.Count
      cur2 <- (cur2 + 1 + prev.[cur2]) % prev.Count
  }
  |> Seq.take (data + scoreAmount)
  |> Seq.rev
  |> Seq.take scoreAmount
  |> Seq.rev
  |> Seq.toList

let part2() =
  seq {
    let prev = ResizeArray()
    prev.Add(3)
    prev.Add(7)
    let mutable cur1 = 0
    let mutable cur2 = 1
    yield 3
    yield 7
    while true do
      let sum = prev.[cur1] + prev.[cur2]
      if sum > 9 then
        prev.Add (sum / 10)
        yield (sum / 10)
        prev.Add (sum % 10)
        yield (sum % 10)
      else
        prev.Add (sum)
        yield sum
      cur1 <- (cur1 + 1 + prev.[cur1]) % prev.Count
      cur2 <- (cur2 + 1 + prev.[cur2]) % prev.Count
  }
  |> Seq.scan (fun (_::ss) i -> ss @ [i]) [0;0;0;0;0;0]
  |> Seq.takeWhile (fun s -> s<>data2)
  |> Seq.length
  |> (fun x -> x - 6)

[<EntryPoint>]
let main argv =
  let s = System.Diagnostics.Stopwatch()
  s.Start()
  let result = part2 ()
  s.Stop()
  printfn "Time: %dms, Result: %A" s.ElapsedMilliseconds result
  0 // return an integer exit code
