module Day9
open System
open System.Collections.Generic

let playerCount = 470
let lastMarble = 7217000L

// let playerCount = 13
// let lastMarble = 7999

let rec cAdd (n:LinkedListNode<int64>) b =
  if b = 0 then
    n
  else if not <| isNull n.Next then
    cAdd n.Next (b-1)
  else
    cAdd n.List.First (b-1)

let rec cRemove (n:LinkedListNode<int64>) b =
  if b = 0 then
    n
  else if not <| isNull n.Previous then
    cRemove n.Previous (b-1)
  else
    cRemove n.List.Last (b-1)
    

let part1 () =
  let startBoard = LinkedList<int64>()
  startBoard.AddFirst 0L |> ignore
  startBoard.AddLast 1L |> ignore
  [2L..lastMarble]
  |> Seq.fold (fun (cp, (cmi: LinkedListNode<int64>), s) m ->
    //printfn "%A, %A, %A, %A" cp cmi s (cmi.List |> Seq.map string |> String.concat " ")
    match m with
    | _ when m % 23L = 0L ->
      let mToRemove = cRemove cmi 7
      let score = m + mToRemove.Value
      let next = cAdd mToRemove 1
      mToRemove.List.Remove mToRemove

      let playerScore = s |> Map.tryFind cp |> Option.defaultValue 0L
      let newS = s |> Map.add cp (playerScore+score)

      ((cp+1) % playerCount, next, newS)
    | _ ->
      let newCmi = (cAdd cmi 1) |> fun newcmi -> newcmi.List.AddAfter(newcmi, m)
      ((cp+1) % playerCount, newCmi, s)
  ) (0, startBoard.Last, Map.empty)
  |> (fun (_,_,s) ->
    s |> Map.toSeq |> Seq.sortByDescending snd
  )

[<EntryPoint>]
let main argv =
    let s = System.Diagnostics.Stopwatch()
    s.Start()
    let result = part1()
    s.Stop()
    printfn "time: %d, result: %A" s.ElapsedMilliseconds result
    0 // return an integer exit code
